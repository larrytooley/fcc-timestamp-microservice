'use strict'

const express = require('express')
const path = require('path')
const moment = require('moment')

var app = express()

app.set('port', (process.env.PORT || 5000));
app.use(express.static('./'));


app.get('/:timestamp', (req, res) => {

  var date = moment(req.params.timestamp, "MMMM DD, YYYY", true)

  if (!date.isValid()) {
    date = moment(req.params.timestamp, "X", true)
  }
  if (!date.isValid()) {
    res.json({
      "unix": null,
      "natural": null
    })
  } else {
    res.json({
      "unix": date.format('X'),
      "natural": date.format('MMMM DD, YYYY')
    })
  }
})

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'))
  console.log(req.headers)
})

app.listen(app.get('port'), function () {
  console.log(`Example app listening on port ${app.get('port')}!`)
})